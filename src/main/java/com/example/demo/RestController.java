package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.web.bind.annotation.RestController
@CrossOrigin
@RequestMapping("/api/demo")
public class RestController {

	@GetMapping
	public String getMessage() {
		
		return "Hello from REST API";
		
	}
	
}
